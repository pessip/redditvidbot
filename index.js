const snoowrap = require("snoowrap");
const shelljs = require("shelljs");
const webshot = require("node-webshot");

const credentials = require("./credentials.json");

const r = new snoowrap(credentials);

r.getTop('TIFU').then((submissions) => {
    submissions.forEach((submission) => {
        makeaudio(submission);
        makeimage('https://old.reddit.com' + submission.permalink, 'out/images/' + submission.id + '.png')
        makeclip(submission.id);
    });
});
function makeaudio(submission) {
    submission.fetch().then((submission) => {
        shelljs.exec('espeak --stdout -s 165 "' + submission.selftext + submission.body + '" > out/audio/' + submission.id + '.wav');
    })
} 

function makeimage(url, file) {
    var config = {
        screenSize: {
            width: 1280,
            height: 720
        }
    }
    webshot(url, file, config)
}

function makeclip(name) {
    shelljs.exec('ffmpeg -loop 1 -i out/images/' + name + '.png -i out/audio/' + name + '.wav -c:v libx264 -tune stillimage -c:a aac -b:a 192k -pix_fmt yuv420p -shortest out/video/clips/' + name + '.mp4')
}